using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLogic : MonoBehaviour
{

    [SerializeField] GameObject door;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            door.GetComponent<Animator>().Play("DoorAnim");
            this.GetComponentInParent<Animator>().Play("ButtonAnim");
        }
    }
}
