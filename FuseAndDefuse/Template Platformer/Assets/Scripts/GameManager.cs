﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{ 
    public delegate void Fusion();
    public Fusion _FusionEvent;
    public delegate void Defuse();
    public Defuse _DefuseEvent;

    public static int actualPlayer = 0; // el jugador actual que maneja
    public int playerNinList;
    public List<GameObject> players; // lista de jugadores   

    public static GameManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {

        _FusionEvent += CheckActivePlayers;
        _DefuseEvent += CheckActivePlayers;

        Physics.gravity = new Vector3(0, -30, 0); // setea gravedad del juego
        CheckActivePlayers();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        OnSceneEnter();
    }

    private void OnSceneEnter()
    {
        CheckActivePlayers();
        Debug.Log("GameManager entered a new scene.");
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput() // lo uso pero con 1 y 2 
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneContoller.instance.ResetLvl();
        }

        if (players.Count >= 1)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) // se mueva uno a la izquierda en la lista de jugadores 
            {
                if (actualPlayer == 0 && players.Count > 1) // si esta en el 0 que pase al ultimo y si es el ultimo de la lista que pase al primero 
                {                   
                    int lastPlayer = players.Count - 1;
                    actualPlayer = lastPlayer; // Que el jugador actual sea el ultimo de la lista
                }
                else
                {
                    actualPlayer--;
                }
                SetConstraits();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
            {
                if (actualPlayer == players.Count - 1)
                {
                    actualPlayer = 0;
                }
                else
                {
                    actualPlayer++;
                }
                SetConstraits();
            }
        }
    } // eleccion de personaje

    private void SetConstraits()
    {
        Debug.Log("se aplicaron constraits");
        foreach (GameObject p in players)
        {
            if (p == players[actualPlayer])
            {
                p.GetComponent<Controller_Player>().enabled = true;
                p.GetComponent<Controller_Player>().ActivatePlayer();
            }
            if(p != players[actualPlayer])
            {
                p.GetComponent<Controller_Player>().DesactivatePlayer();
                p.GetComponent<Controller_Player>().enabled = false;       
            }
        }
    } // bloquear mov para los otros personajes

    public void CheckActivePlayers() // asigno valor en la lista a cada player
    {
        playerNinList = 0;
        players.Clear();
        players.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        players.Sort((p1, p2) => p1.name.CompareTo(p2.name));
        foreach (GameObject p in players)
        {
            if (p == players[0])
            {
                p.GetComponent<Controller_Player>().playerNumber = playerNinList;
            }
            else
            {
                p.GetComponent<Controller_Player>().playerNumber = (playerNinList += 1);
            }
        }
        SetConstraits();
    }  

    public IEnumerator InvokeFusion()
    {
        yield return new WaitForSeconds(0.15f);
        _FusionEvent();
    }
    public IEnumerator InvokeDefusion()
    {
        yield return new WaitForSeconds(0.15f);
        _DefuseEvent();
    }
}
