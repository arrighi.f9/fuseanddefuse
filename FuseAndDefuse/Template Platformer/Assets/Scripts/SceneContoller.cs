﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneContoller : MonoBehaviour
{

    public static SceneContoller instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void ResetLvl()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadNextLvl()
    {
        int sceneIdex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sceneIdex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
