﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private bool isShooting;
    [SerializeField]
    private GameObject bulletPos;
    [SerializeField]
    private GameObject bullet;
    private float bulletVelocity = 500;
    private float timeBeetweenShoots;
    private GameObject Enemy;

    private void Start()
    {
        isShooting = false;
        Enemy = this.gameObject;
        timeBeetweenShoots = 1.5f;
    }

    public void Update()
    {
        if (Enemy != null && gameObject.GetComponent<GuardianMechanic>().Chassing == true)
        {
            ShootBullet();
        }
    }
    public void ShootBullet()
    {
        if (isShooting == false)
        {
            GameObject BUllet = Instantiate(bullet, bulletPos.gameObject.transform.position, bulletPos.gameObject.transform.rotation) as GameObject; //shoots from enemies eyes
            Rigidbody rb = BUllet.GetComponent<Rigidbody>();
            rb.AddForce(rb.transform.forward * bulletVelocity);
            Destroy(BUllet, 1.3f);
            isShooting = true;
            Invoke(nameof(ResetShoot), timeBeetweenShoots);
        }
    }
    public void ResetShoot()
    {
        isShooting = false;
    }
}
