﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianMechanic : MonoBehaviour
{
    public List<GameObject> players;
    private GameObject player;
    public float velocity;
    public float vision;
    [SerializeField]
    private Transform[] positions;
    [SerializeField]
    private float MinimumRange;
    private int nextPos = 0;
    
    public bool Chassing = false;

    private void Start()
    {
        players = GameManager.instance.players;
    }

    void Update()
    {
        player = players[GameManager.actualPlayer]; // que persiga solo al player actual
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance < vision && player.CompareTag("Player")) // vea si tambien tiene el tag ( uso en invisibilidad ) 
        {
            ChassingPlayer();
        }
        else 
        {
            Patrolling();
        }
    }

    private void Patrolling()
    {
        Chassing = false;
        transform.position = Vector3.MoveTowards(transform.position, positions[nextPos].position, velocity * Time.deltaTime);  
        if(Vector3.Distance(transform.position,positions[nextPos].position)< MinimumRange) // para que vaya pasando de punto en punto
        {
            nextPos += 1;
            if(nextPos == positions.Length)
            {
                nextPos = 0;
            }
        }
    }

    private void ChassingPlayer()
    {       
        Chassing = true;
        transform.LookAt(player.transform);
        transform.Translate(velocity * Vector3.forward * Time.deltaTime);
    }
}
