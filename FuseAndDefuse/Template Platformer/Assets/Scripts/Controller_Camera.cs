﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<GameObject> players;
    private Camera _camera;
    public float dampTime = 0.15f;
    public float smoothTime = 2f;
    public float zoomvalue;
    private Vector3 velocity = Vector3.zero;


    void Start()
    {
        players = GameManager.instance.players;
        _camera = GetComponent<Camera>();
    }

    void LateUpdate()
    {
        if (players[GameManager.actualPlayer] != null)
        {          
            // que centre la mira en el jugador
            Vector3 point = _camera.WorldToViewportPoint(players[GameManager.actualPlayer].transform.position);
            // El offset
            Vector3 delta = players[GameManager.actualPlayer].transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
          // pos del jugador + la diferencia que hay entre el y la camara
            Vector3 destination = transform.position + delta;
            // que haga la transicion de una pos a otra
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }
        
    }
}
