﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Controller_Target : MonoBehaviour
{  
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            SceneContoller.instance.LoadNextLvl();
        }
    }
}
