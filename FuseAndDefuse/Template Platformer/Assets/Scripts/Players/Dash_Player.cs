﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash_Player : Controller_Player
{
    public bool canDash;
    public float dashTime;
    public bool isDashing;
    public float dashSpeed;
    public float dashingCoolDown;
    public bool isLookingR = false;
    public bool isLookingL = false;

    public Dash_Player()
    {
        canFuse = true;
        canDefuse = false;
        canDash = true;
        isDashing = false;
        playerType = Type.Dash;
    }

    public override void Movement()
    {
        if (isDashing) // si estoy en dash que no puedo moverme
        {
            return;
        }
        if (Input.GetKey(KeyCode.A) && canMoveLeft)
        {
            rb.velocity = new Vector3(1 * -speed, rb.velocity.y, 0);
            isLookingL = true;
            isLookingR = false;
            if (Input.GetKey(KeyCode.Space) && canDash == true)
            {
                StartCoroutine(DashLeft());
                              
            }
        }
        else if (Input.GetKey(KeyCode.D) && canMoveRight)
        {
            rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
            isLookingR = true;
            isLookingL = false;
            if (Input.GetKey(KeyCode.Space) && canDash == true)
            {
                StartCoroutine(DashRight());                          
            }
        }
        else
        {
            isLookingL = false;
            isLookingR = false;
        }         
    }
    public override void FixedUpdate()
    {
        if (isDashing)
        {
            return;
        }
        Movement();
        
    }
    IEnumerator DashRight()
    {
        canDash = false;
        isDashing = true;
        rb.velocity = new Vector3(transform.localScale.x * dashSpeed, 0);
        yield return new WaitForSeconds(dashTime);
        isDashing = false;
        yield return new WaitForSeconds(dashingCoolDown);
        canDash = true;
    }
    IEnumerator DashLeft()
    {
        canDash = false;
        isDashing = true;
        rb.velocity = new Vector3(transform.localScale.x * -dashSpeed, 0);
        yield return new WaitForSeconds(dashTime);
        isDashing = false;
        yield return new WaitForSeconds(dashingCoolDown);
        canDash = true;
    }
}
