﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invi_Player : Controller_Player
{
    private int maxTime;
    public float actualTime;
    public GameObject timeTxt;
    public Material[] mat;

    public Invi_Player()
    {
        canFuse = true;
        canDefuse = false;
        playerType = Type.Invisible;
        jumpForce = 12;
    }
    public override void Start()
    {
        base.Start();
        this.gameObject.GetComponent<Renderer>().sharedMaterial = mat[0];
        maxTime = 3;
        actualTime = maxTime;
    }

    public override void Update()
    {
        base.Update();
        if (Input.GetKey(KeyCode.Space))
        {
            Invisible();
        }
        else
        {
            ResetTime();
        }

        if (actualTime <= 0)
        {
            ResetTime();
        }
        timeTxt.GetComponent<TextMesh>().text = Mathf.Round(actualTime).ToString();
    }

    public void Invisible()
    {
        this.gameObject.tag = "Invisible";
        actualTime -= Time.deltaTime;
        this.gameObject.GetComponent<Renderer>().sharedMaterial = mat[1];
    }
    public void ResetTime()
    {
        this.gameObject.tag = "Player";
        this.gameObject.GetComponent<Renderer>().sharedMaterial = mat[0];
        if (actualTime < maxTime)
        {
            actualTime += Time.deltaTime;
        }
    }
}
