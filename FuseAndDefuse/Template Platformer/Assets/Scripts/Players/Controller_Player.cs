﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    internal enum Type { Dash, DoubleJump, Invisible, Light, Gravity}
    internal Type playerType;

    [SerializeField]
    internal GameObject PlayerToCreate; // el que creo cuando se fusionan

    [SerializeField]
    private GameObject PlayerToDivide; // los que creo cuando se divide

    internal float jumpForce = 10;

    internal float speed = 5;

    public int playerNumber;

    internal Rigidbody rb;

    internal RaycastHit leftHit,rightHit,downHit;

    internal bool canMoveLeft, canMoveRight; // chequea si me puedo mover en las direcciones
    internal bool onFloor;

    [SerializeField]
    internal bool canDefuse; // DoubleJump y Light pueden
    [SerializeField]
    internal bool canFuse; // DoubleJump y Light pueden

    public virtual void Start()
    {
        canMoveLeft = true;
        canMoveRight = true;
        rb = GetComponent<Rigidbody>();
        if (playerNumber == 0)
        {
            GameManager.actualPlayer = playerNumber;
        }
    }

    public virtual void FixedUpdate()
    {
        Movement();
    }

    public virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && canDefuse == true)
        {
            DefusePlayer();
            GameManager.instance._DefuseEvent();
        }
        if (IsOnSomething())
        {
            onFloor = true;
        }
        else
        {
            onFloor= false;
        }
        Jump();
    }

    public virtual bool IsOnSomething()
    { // crea una caja y chequea los hits. para ver si esta encima de algo
        RaycastHit hit;
        return Physics.Raycast(transform.position, -transform.up, out hit, (transform.localScale.y / 2) + 0.1f); 
    }

    public virtual void Movement()
    {
        if (Input.GetKey(KeyCode.A) && canMoveLeft)
        {        
            rb.velocity = new Vector3(1 * -speed, rb.velocity.y, 0);         
        }
        else if (Input.GetKey(KeyCode.D) && canMoveRight)
        {
            rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);              
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }

    public virtual void Jump()  // salto
    {
        if (Input.GetKeyDown(KeyCode.W) && this.playerType != Type.Gravity)
        {
            if (onFloor)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void DesactivatePlayer()
    {
        Debug.Log("se desactivo: " + this.gameObject.name);
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }
    public void ActivatePlayer()
    {
        Debug.Log("se activo: " + this.gameObject.name);
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }

    public void OnCollisionEnter(Collision collision) // para el agua que pierda y el piso para salto 
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<Controller_Player>().playerType == this.playerType && canFuse == true && collision.gameObject.GetComponent<Controller_Player>().enabled == true) // toma la colision del que no estoy usando 
            {
                Instantiate(PlayerToCreate, this.transform.position, Quaternion.identity);
                Destroy(collision.gameObject);
                Debug.Log("Se instancio desde " + this.gameObject.name);
                FusePlayers();
            }
        }   
    }

    public void FusePlayers()
    {
        GameManager.instance.StartCoroutine("InvokeFusion", 0);
        Destroy(this.gameObject);
    }
    
    public void DefusePlayer()
    {
        Instantiate(PlayerToDivide,new Vector3(this.transform.position.x - 1, transform.position.y, transform.position.z), Quaternion.identity);
        Instantiate(PlayerToDivide, new Vector3(this.transform.position.x + 1, transform.position.y, transform.position.z), Quaternion.identity);
        GameManager.instance.StartCoroutine("InvokeDefusion", 0);
        Destroy(this.gameObject);
    }
    public IEnumerator FreezePos(float seconds)
    {
        DesactivatePlayer();
        yield return new WaitForSeconds(seconds);
        ActivatePlayer();
    }
}
