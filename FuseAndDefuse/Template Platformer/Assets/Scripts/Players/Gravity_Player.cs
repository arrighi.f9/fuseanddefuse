﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity_Player : Controller_Player
{
    public bool gravityActivated = true; // true  es que esta invertido 
    public float gravity;

    public Gravity_Player()
    { 
        canDefuse = true;
        canFuse = false;
        gravityActivated = true;
        playerType = Type.Gravity;
    }
    public override void Update()
    {
        base.Update();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (gravityActivated == true)
            {
                gravity = -500f;
                gravityActivated = false;
            }
            else
            {
                if (gravityActivated == false)
                {
                    gravity = 500f;
                    gravityActivated = true;         
                }
            }
        }
        rb.AddForce(new Vector3(0, gravity * Time.deltaTime, 0));// le agrego la "gravedad" 
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate(); // y ahora si lo muevo con el metodo de la clase based        
    }
}
