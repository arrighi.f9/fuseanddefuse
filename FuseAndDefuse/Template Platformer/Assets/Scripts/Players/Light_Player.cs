﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Light_Player : Controller_Player
{
    
    public GameObject objLight;
    public int maxTime;
    public float actualTime;   
    public GameObject timeTxt;

    public Light_Player()
    {
        canDefuse = true;
        canFuse = false;
        playerType = Type.Invisible;
    }
    public override void Start()
    {
        base.Start();
        objLight.SetActive(false);
        maxTime = 3;
        actualTime = maxTime;
    }

    public override void Update()
    {
        base.Update();
            if (Input.GetKey(KeyCode.Space))
            {
                ActivateLight();
            }
            else
            {
                ResetTime();
            }
            if (actualTime <= 0)
            {
                ResetTime();
            }
        
        timeTxt.GetComponent<TextMesh>().text = Mathf.Round(actualTime).ToString();      
    }

    public void ActivateLight()
    {
        objLight.SetActive(true);
        actualTime -= Time.deltaTime;
    }

    public void ResetTime()
    {
        objLight.SetActive(false);       
        if (actualTime < maxTime)
        {
            actualTime += Time.deltaTime;
        }              
    }
}
