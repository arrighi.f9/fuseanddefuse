﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DJump_Player : Controller_Player
{
    private int jumpCounter = 0;

    public DJump_Player()
    {
        canDefuse = true;
        canFuse = true;
        jumpCounter = 0;
        playerType = Type.DoubleJump;
    }
    public override void Jump()
    {
        if (onFloor)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                jumpCounter = 1;
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (jumpCounter > 0)
                {
                    rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                    jumpCounter--;
                }
            }
        }
    }
}
